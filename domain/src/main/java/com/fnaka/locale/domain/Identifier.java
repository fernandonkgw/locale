package com.fnaka.locale.domain;

public abstract class Identifier extends ValueObject {

    public abstract String getValue();
}
