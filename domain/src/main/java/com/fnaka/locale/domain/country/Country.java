package com.fnaka.locale.domain.country;

import com.fnaka.locale.domain.AggregateRoot;
import com.fnaka.locale.domain.utils.InstantUtils;

import java.time.Instant;

public class Country extends AggregateRoot<CountryID> {

    private String name;
    private boolean active;
    private Instant createdAt;
    private Instant updatedAt;
    private Instant deletedAt;

    protected Country(
            final CountryID countryID,
            final String name,
            final boolean active,
            final Instant createdAt,
            final Instant updatedAt,
            final Instant deletedAt
    ) {
        super(countryID);
        this.name = name;
        this.active = active;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }

    public static Country newCountry(final String aName, final boolean isAtive) {
        final var anId = CountryID.unique();
        final var now = InstantUtils.now();
        final var deletedAt = isAtive ? null : now;
        return new Country(anId, aName, isAtive, now, now, deletedAt);
    }

    public String getName() {
        return name;
    }

    public boolean isActive() {
        return active;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public Instant getDeletedAt() {
        return deletedAt;
    }
}
