package com.fnaka.locale.domain.country;

import com.fnaka.locale.domain.Identifier;
import com.fnaka.locale.domain.utils.IdUtils;

import java.util.Objects;

public class CountryID extends Identifier {

    private final String value;

    private CountryID(final String value) {
        this.value = Objects.requireNonNull(value);
    }

    public static CountryID from(final String anId) {
        return new CountryID(anId);
    }

    public static CountryID unique() {
        return CountryID.from(IdUtils.uuid());
    }

    @Override
    public String getValue() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final CountryID countryID = (CountryID) o;
        return getValue().equals(countryID.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }
}
