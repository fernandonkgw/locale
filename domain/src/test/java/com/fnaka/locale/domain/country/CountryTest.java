package com.fnaka.locale.domain.country;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CountryTest {

    @Test
    void givenValidParams_whenCallsNewCountry_shouldInstantiateIt() {
        // given
        final var expectedName = "Brasil";
        final var expectedIsActive = true;

        // when
        final var actualCountry = Country.newCountry(expectedName, expectedIsActive);

        // then
        Assertions.assertNotNull(actualCountry);
        Assertions.assertNotNull(actualCountry.getId());
        Assertions.assertEquals(expectedName, actualCountry.getName());
        Assertions.assertEquals(expectedIsActive, actualCountry.isActive());
        Assertions.assertNotNull(actualCountry.getCreatedAt());
        Assertions.assertNotNull(actualCountry.getUpdatedAt());
        Assertions.assertNull(actualCountry.getDeletedAt());
    }

    @Test
    void givenValidParamInactive_whenCallsNewCountry_shouldInstantiateIt() {
        // given
        final var expectedName = "Brasil";
        final var expectedIsActive = false;

        // when
        final var actualCountry = Country.newCountry(expectedName, expectedIsActive);

        // then
        Assertions.assertNotNull(actualCountry);
        Assertions.assertNotNull(actualCountry.getId());
        Assertions.assertEquals(expectedName, actualCountry.getName());
        Assertions.assertEquals(expectedIsActive, actualCountry.isActive());
        Assertions.assertNotNull(actualCountry.getCreatedAt());
        Assertions.assertNotNull(actualCountry.getUpdatedAt());
        Assertions.assertNotNull(actualCountry.getDeletedAt());
    }
}
